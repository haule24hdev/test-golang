package main

import (
	"fmt"
	"math"
	"math/rand"
	"strconv"
	"strings"
	"time"
	"unicode"
)

func isASCII(s string) bool {
	for _, c := range s {
		if c > unicode.MaxASCII {
			return false
		}
	}
	return true
}

/*
	testValidity that takes the string as an input, and returns boolean flag true if the given string complies with the
		format, or false if the string does not comply
	difficulty: 2/5
	estimated time: 20 min
	implement time: 16 min
*/
func testValidity(input string) bool {
	if input == "" {
		return false
	}

	listText := strings.Split(input, "-")
	if len(listText) < 2 {
		return false
	}
	for i := 0; i < len(listText)-1; i += 2 {
		num, err := strconv.Atoi(listText[i])
		if err != nil {
			return false
		}
		if num < 0 {
			return false
		}
		if len(listText[i+1]) == 0 || !isASCII(listText[i+1]) {
			return false
		}
	}
	return true
}

/*
	averageNumber that takes the string, and returns the average number from all the numbers
	difficulty: 2/5
	estimated time: 5 min
	implement time: 5 min
*/
func averageNumber(input string) int {
	if !testValidity(input) {
		return 0
	}

	total := 0
	count := 0
	listText := strings.Split(input, "-")
	for i := 0; i < len(listText)-1; i += 2 {
		num, err := strconv.Atoi(listText[i])
		if err == nil {
			total += num
			count++
		}
	}
	return total / count
}

/*
	wholeStory that takes the string, and returns a text that is composed from all the text words separated by spaces,
		e.g. story called for the string 1-hello-2-world should return text: "hello world"
	difficulty: 2/5
	estimated time: 5 min
	implement time: 5 min
*/
func wholeStory(input string) string {
	if !testValidity(input) {
		return ""
	}
	listText := strings.Split(input, "-")
	texts := make([]string, 0)
	for i := 1; i < len(listText); i += 2 {
		texts = append(texts, listText[i])
	}
	return strings.Join(texts, " ")
}

/*
	storyStats that returns four things:
	-the shortest word
	-the longest word
	-the average word length
	-the list (or empty list) of all words from the story that have the length the same as the average length rounded up and down.
	difficulty: 3/5
	estimated time: 20 min
	implement time:  15 min
*/
func storyStats(input string) (shortest string, longest string, average float64, list []string) {
	if !testValidity(input) {
		return "", "", 0, []string{}
	}
	listText := strings.Split(input, "-")
	texts := make([]string, 0)
	shortest = ""
	longest = ""
	totalLen := 0
	for i := 1; i < len(listText); i += 2 {
		item := listText[i]
		if len(item) <= len(shortest) || shortest == "" {
			shortest = item
		}
		if len(item) >= len(longest) {
			longest = item
		}
		totalLen += len(item)
		texts = append(texts, listText[i])
	}
	average = float64(totalLen) / float64(len(texts))
	for _, item := range texts {
		if len(item) == int(math.Round(average)) {
			list = append(list, item)
		}
	}
	return shortest, longest, average, list
}

const MAX_PART = 5
const MAX_NUMBER = 10
const MAX_LEN_TEXT = 4

/*
	generateText a generate function, that takes boolean flag and generates random correct strings if the parameter is true
		and random incorrect strings if the flag is false.
	difficulty: 4/5
	estimated time: 30 min
	implement time:  35 min
*/
func generateText(flag bool) string {
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(MAX_PART) + 1
	result := ""
	if flag {
		for i := 0; i < n*2; i += 2 {
			rand.Seed(time.Now().UnixNano())
			randNumber := rand.Intn(MAX_NUMBER) + 1
			rand.Seed(time.Now().UnixNano())
			randLenText := rand.Intn(MAX_LEN_TEXT) + 1
			randText := randomText(randLenText, true)
			result += strconv.Itoa(randNumber) + "-" + randText + "-"
		}
		return result[:len(result)-1]
	}
	for i := 0; i < n*2; i += 2 {
		rand.Seed(time.Now().UnixNano())
		randLenText := rand.Intn(MAX_LEN_TEXT) + 1
		result += randomText(randLenText, false)
	}
	return result
}

func randomText(n int, flag bool) string {
	result := ""
	maxKey := 8777
	if flag {
		maxKey = unicode.MaxASCII

	}
	for i := 0; i < n; i++ {
		rand.Seed(time.Now().UnixNano())
		randASCII := rand.Intn(maxKey)
		key := string(rune(randASCII))
		if key != "-" && key != "" {
			result += key
		}
	}
	return result
}

func main() {
	fmt.Println(generateText(false))
}
