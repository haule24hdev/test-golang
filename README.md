# Test
This is a project about test
## Running the test covered

---
We run the following two commands to build the coverage file
```bash
go test ./... -cover -coverprofile=c.out
```
```bash
go tool cover -html=c.out -o coverage.html
```
You can open coverage.html in browser check test covered