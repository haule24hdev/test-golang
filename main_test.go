package main

import (
	"testing"
)

func Test_TestValidity(t *testing.T) {
	t.Run("Test_TestValidity_Valid", func(t *testing.T) {
		input := generateText(true)
		result := testValidity(input)
		if !result {
			t.Errorf("Test_TestValidity_Valid expect true instead of %v", result)
		}
	})
	t.Run("Test_TestValidity_Invalid", func(t *testing.T) {
		input := generateText(false)
		result := testValidity(input)
		if result {
			t.Errorf("Test_TestValidity_Valid expect false instead of %v", result)
		}
	})
	t.Run("Test_TestValidity_Invalid_Case2", func(t *testing.T) {
		result := testValidity("")
		if result {
			t.Errorf("Test_TestValidity_Valid_Case2 expect false instead of %v", result)
		}
	})
	t.Run("Test_TestValidity_Invalid_Case3", func(t *testing.T) {
		result := testValidity("23s2-sdsd")
		if result {
			t.Errorf("Test_TestValidity_Invalid_Case3 expect false instead of %v", result)
		}
	})
	t.Run("Test_TestValidity_Invalid_Case4", func(t *testing.T) {
		result := testValidity("-23s2-sdsd")
		if result {
			t.Errorf("Test_TestValidity_Invalid_Case4 expect false instead of %v", result)
		}
	})
}

func Test_averageNumber(t *testing.T) {
	t.Run("Test_averageNumber_case1", func(t *testing.T) {
		result := averageNumber("2-sdj-3-dfjs-1-sd")
		if result != 2 {
			t.Errorf("Test_averageNumber_case1 expect 3 instead of %v", result)
		}
	})
	t.Run("Test_averageNumber_case2", func(t *testing.T) {
		result := averageNumber("sdsdsd")
		if result != 0 {
			t.Errorf("Test_averageNumber_case2 expect 0 instead of %v", result)
		}
	})
}

func Test_wholeStory(t *testing.T) {
	t.Run("Test_wholeStory_case1", func(t *testing.T) {
		result := wholeStory("2-sdj-3-dfjs-1-sd")
		if result != "sdj dfjs sd" {
			t.Errorf("Test_wholeStory_case1 expect 3 instead of %v", result)
		}
	})
	t.Run("Test_wholeStory_case2", func(t *testing.T) {
		input := generateText(false)
		result := wholeStory(input)
		if result != "" {
			t.Errorf("Test_wholeStory_case2 expect \"\" instead of %v", result)
		}
	})
}

func Test_storyStats(t *testing.T) {
	t.Run("Test_storyStats_case1", func(t *testing.T) {
		shortest, longest, average, list := storyStats("2-sdj-3-dfjs-1-sd")
		if shortest != "sd" {
			t.Errorf("Test_wholeStory_case1 expect shortest = \"sd\" instead of \"%v\"", shortest)
		}
		if longest != "dfjs" {
			t.Errorf("Test_wholeStory_case1 expect longest = \"sd\" instead of \"%v\"", longest)
		}
		if average != 3 {
			t.Errorf("Test_wholeStory_case1 expect average = 4 instead of %v", average)
		}
		if len(list) != 1 && list[0] != "sdj" {
			t.Errorf("Test_wholeStory_case1 expect list = [sdj] instead of %v", list)
		}
	})
	t.Run("Test_storyStats_case2", func(t *testing.T) {
		shortest, longest, average, list := storyStats("232-Ϙᙬ")
		if shortest != "" {
			t.Errorf("Test_wholeStory_case1 expect shortest = \"sd\" instead of \"%v\"", shortest)
		}
		if longest != "" {
			t.Errorf("Test_wholeStory_case1 expect longest = \"sd\" instead of \"%v\"", longest)
		}
		if average != 0 {
			t.Errorf("Test_wholeStory_case1 expect average = 4 instead of %v", average)
		}
		if len(list) != 0 {
			t.Errorf("Test_wholeStory_case1 expect list = [] instead of %v", list)
		}
	})
}
